// Sort through primary key
function getPosts() {
  var recentPostId = $(".postList .posts").first().attr("id");
  $.ajax({
    url: '/update-post/' + recentPostId,
    type: "POST",
    dataType: "html",
    success: function(resp) {
      $(".postList").prepend(resp);
      console.log("updated posts: success");
    },
    error: function( xhr, status, errorThrown ) {
      console.log( "Sorry, there was a problem!" );
      console.log( "Error: " + errorThrown );
      console.log( "Status: " + status );
      console.dir( xhr );
    }
  });
}

$(document).ready(function() {
  if ($(".posts").length > 0) {
    window.setInterval(getPosts, 10000);
  }
});