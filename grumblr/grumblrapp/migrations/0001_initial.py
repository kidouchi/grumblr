# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='GrumblrComment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comment', models.CharField(max_length=200)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GrumblrPost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('image', models.ImageField(null=True, upload_to=b'grumblr_pics', blank=True)),
                ('textpost', models.CharField(max_length=400)),
                ('posted_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ('-id',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GrumblrProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('img', models.ImageField(default=b'/media/user_pics/user_default.png', upload_to=b'user_pics', blank=True)),
                ('email', models.EmailField(max_length=75, blank=True)),
                ('age', models.PositiveSmallIntegerField(null=True, blank=True)),
                ('birthday', models.DateField(null=True, blank=True)),
                ('self_description', models.CharField(max_length=400, blank=True)),
                ('blocked_users', models.ManyToManyField(related_name=b'blocked_by', to='grumblrapp.GrumblrProfile', blank=True)),
                ('followers', models.ManyToManyField(related_name=b'following', to='grumblrapp.GrumblrProfile', blank=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='grumblrpost',
            name='dislikes',
            field=models.ManyToManyField(default=b'', to='grumblrapp.GrumblrProfile', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='grumblrpost',
            name='profile',
            field=models.ForeignKey(related_name=b'post_profile', to='grumblrapp.GrumblrProfile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='grumblrcomment',
            name='post',
            field=models.ForeignKey(related_name=b'comments', on_delete=django.db.models.deletion.SET_NULL, to='grumblrapp.GrumblrPost', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='grumblrcomment',
            name='profile',
            field=models.ForeignKey(to='grumblrapp.GrumblrProfile'),
            preserve_default=True,
        ),
    ]
