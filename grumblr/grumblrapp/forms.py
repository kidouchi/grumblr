import sys
from django import forms

from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm, SetPasswordForm
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User

from django.forms import ModelForm
from grumblrapp.models import *

class ResetPasswordForm(PasswordResetForm):
    email = forms.EmailField(label = 'Email',
                                widget = forms.EmailInput(
                                    attrs = {'class' : 'form-control'}))

class NewSetPasswordForm(SetPasswordForm):
    new_password1 = forms.CharField(label = "New password",
                                        widget = forms.PasswordInput(
                                            attrs = {'class' : 'form-control'}))
    new_password2 = forms.CharField(label = "New password confirmation",
                                        widget = forms.PasswordInput(
                                            attrs = {'class' : 'form-control'}))

class RegistrationForm(forms.Form):
    username = forms.CharField(max_length = 20,
    			                 widget = forms.TextInput(
    							 attrs={'class' : 'form-control',
    									'placeholder' : 'New Username'}))
    password1 = forms.CharField(max_length = 200, 
    							label = 'Password',
                                widget = forms.PasswordInput(
                                attrs={'class' : 'form-control',
                                		   'placeholder' : 'New Password'}))
    password2 = forms.CharField(max_length = 200,
    							label = 'Confirm Password',
                                widget = forms.PasswordInput(
                                	attrs={'class' : 'form-control',
                                		   'placeholder' : 'Retype Password'}))
    email = forms.EmailField(max_length=200,
                             label = 'Email',
                             widget = forms.EmailInput(
                                attrs = {'class' : 'form-control',
                                        'placeholder' : 'Your Email Here...'}))

    def clean(self):
        cleaned_data = super(RegistrationForm, self).clean()

        password1 = cleaned_data.get('password1')
        password2 = cleaned_data.get('password2')
        if (password1 and password2 and password1 != password2):
            raise forms.ValidationError("Passwords did not match.")

        return cleaned_data

    def clean_username(self):
        username = self.cleaned_data.get('username')
        if User.objects.filter(username__exact=username):
            raise forms.ValidationError("Username is already taken.")

        return username

class LoginForm(AuthenticationForm):
    username = forms.CharField(widget = forms.TextInput(
        attrs={'class': 'form-control',
               'placeholder' : 'Username',
        }))
    password = forms.CharField(widget = forms.PasswordInput(
        attrs={'class' : 'form-control',
               'placeholder' : 'Password',
        }))

class GrumblrPostForm(ModelForm):
    class Meta:
        model = GrumblrPost
        fields = '__all__'
        exclude = ['profile', 'dislikes', 'posted_at']
        labels = {
            'title' : 'Post Title',
            'textpost' : 'Description',
            'image' : 'Attach Image'
        }
        widgets = {
            'image' : forms.FileInput(),
            'title' : forms.TextInput(attrs={'class' : 'form-control', 'id' : 'title',
                'placeholder' : 'Max 100 character limit'}),
            'textpost' : forms.Textarea(attrs={'class' : 'form-control',
                'id' : 'description', 'placeholder' : 'Max 400 character limit', 
                'maxlength' : 400, 'rows' : 4})
        }

class GrumblrProfileForm(ModelForm):
    email1 = forms.CharField(label='New Email Address',
                            required=False,
                            widget=forms.EmailInput(
                                attrs={'class' : 'form-control',
                                        'id' : 'InputEmail1',
                                        'placeholder' : 'Type Email Address...'}))
    email2 = forms.CharField(label='Retype Email Address',
                            required=False,
                            widget=forms.EmailInput(
                                attrs={'class' : 'form-control',
                                        'id' : 'InputEmail2',
                                        'placeholder' : 'Retype Email Address...'}))
    password = forms.CharField(label='Current Password',
                                required=False,
                                widget=forms.PasswordInput(
                                    attrs={'class' : 'form-control',
                                            'id' : 'inputPassword1',
                                            'placeholder' : 'Type Your Current Password...'}))
    password1 = forms.CharField(label='New Password',
                                required=False,
                                widget=forms.PasswordInput(
                                    attrs={'class' : 'form-control',
                                            'id' : 'inputPassword2',
                                            'placeholder' : 'Type New Password...'}))
    password2 = forms.CharField(label='Confirm Password',
                                required=False,
                                widget=forms.PasswordInput(
                                    attrs={'class' : 'form-control',
                                            'id' : 'inputPassword3',
                                            'placeholder' : 'Retype New Password...'}))

    class Meta:
        model = GrumblrProfile
        fields = '__all__'
        exclude = ['user', 'email', 'followers', 'dislikes']
        labels = {
            'img' : 'Upload Profile Picture',
            'age' : 'Age',
            'birthday' : 'Birthday',
            'self_description' : 'Introduce Yourself!',
        }
        widgets = { 
            'img' : forms.FileInput(),
            'age' : forms.NumberInput(attrs={'class' : 'form-control', 'id' : 'age'}),
            # DateInput is not type=date for some reason?
            'birthday' : forms.DateInput(attrs={'class' : 'form-control', 'id' : 'birthday', 'type' : 'date'}),
            'self_description' : forms.Textarea(attrs={'class' : 'form-control', 'maxlength' : 400, 'id' : 'description', 'placeholder' : '400 max character limit'}),
        }

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(GrumblrProfileForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(GrumblrProfileForm, self).clean()
        if ('upload-pw' in self.data):
            password = cleaned_data['password']            
            password1 = cleaned_data['password1']
            password2 = cleaned_data['password2']

            if (not password or not password1 or not password2):
                raise forms.ValidationError("Password fields are empty")

            if (password and not check_password(password, self.user.password)):
                raise forms.ValidationError("This is not your password")
        
            if (password1 and password2 and password1 != password2):
                raise forms.ValidationError("Passwords did not match")

        if ('upload-email' in self.data):
            email1 = cleaned_data.get('email1')
            email2 = cleaned_data.get('email2')
       
            if (not email1 or not email2):
                raise forms.ValidationError("Email fields are empty")

            if (email1 and email2 and email1 != email2):
                raise forms.ValidationError("Email addresses did not match")    

        if ('upload-info' in self.data):
            age = cleaned_data.get('age')
            birthday = cleaned_data.get('birthday')
            self_description = cleaned_data.get('self_description')

            if (not age and not birthday and not self_description):
                raise forms.ValidationError("No personal info fields were filled")

        return cleaned_data

class GrumblrCommentForm(ModelForm):
    class Meta:
        model = GrumblrComment
        fields = '__all__'
        exclude = ['post', 'profile', 'created_at']
        widgets = {
            'comment' : forms.Textarea(attrs={
                'class' : 'form-control',
                'placeholder' : 'Comment here...',
                'rows' : 2
                })
        }



