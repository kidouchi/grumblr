# ---------------------- LIBRARIES/IMPORTS ---------------------- 
import sys
import urllib
import buttons_views
from itertools import chain

from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.shortcuts import render, redirect, get_object_or_404

# Decorator to use built-in authentication system
from django.contrib.auth.decorators import login_required

# Used to create and manually log in a user
from django.contrib.auth.models import User

from grumblrapp.models import *
from grumblrapp.forms import *
# ----------------------------- MAIN -----------------------------
@login_required
@transaction.atomic
def search_results(request):
   # Will search by post title
   profile = get_object_or_404(GrumblrProfile, user=request.user)
   keyword=''
   if ('search' in request.POST and request.POST['search']):
      keyword = request.POST['search']
   
   find_by_title = GrumblrPost.objects.filter(title__contains=keyword).order_by('-posted_at') 
   find_by_text = GrumblrPost.objects.filter(textpost__contains=keyword).order_by('-posted_at')
   find_by_user = GrumblrPost.objects.filter(profile__user__username=keyword).order_by('-posted_at')
   posts = set(list(chain(find_by_title, find_by_user, find_by_text)))
   
   profile_to_posts = []
   for post in posts:
      # new_profile = get_object_or_404(GrumblrProfile, user=post.user)
      is_owner = post.profile.user.username == request.user.username
      profile_to_posts.append(
         (post, #0
          buttons_views.was_disliked(post, request.user), #1
          buttons_views.was_followed(profile, post.profile.user), #2
          profile.blocked_by.all().filter(user=post.profile.user).exists(), #3
          is_owner #4
         ))
    
   context = {'profile_posts' : profile_to_posts, 'profile' : profile, 
      'form' : GrumblrCommentForm()}
   return render(request, 'grumblrapp/search-results.html', context)
