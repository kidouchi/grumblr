# This is for the follow/unfollow, dislike/like, 
# block/unblock
# ---------------------- LIBRARIES/IMPORTS ---------------------- 
import sys
import urllib

from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.shortcuts import render, redirect, get_object_or_404

# Decorator to use built-in authentication system
from django.contrib.auth.decorators import login_required

# Used to create and manually log in a user
from django.contrib.auth.models import User

# To manually create HttpResponses or raise Http404 exception
from django.http import HttpResponse, Http404

from grumblrapp.models import *
from grumblrapp.forms import *
# ----------------------------- MAIN -----------------------------
# Check if user disliked this post
def was_disliked(post, user):
   if(post.dislikes.all().filter(user=user).exists()):
      return True
   else:
      return False

# Check if profile has this follower(user)
def was_followed(profile, post_user):
   if(profile.followers.all().filter(user=post_user)):
      return True
   else:
      return False

def was_blocked(profile, blocked_user):
   if(profile.blocked_users.filter(user=blocked_user)):
      return True
   else:
      return False


@login_required
@transaction.atomic
def follow(request, username):
   get_user = get_object_or_404(User, username=username)
   user_profile = get_object_or_404(GrumblrProfile, user=get_user)
   # Add follower to user
   profile = get_object_or_404(GrumblrProfile, user=request.user)
   profile.followers.add(user_profile)
   profile.save()

   return redirect(reverse('home-followers'))

@login_required
@transaction.atomic
def unfollow(request, username):
   # Get Follower's user object
   get_user = get_object_or_404(User, username=username)
   get_profile = get_object_or_404(GrumblrProfile, user=get_user)

   # Delete follower from user
   profile = get_object_or_404(GrumblrProfile, user=request.user)
   profile.followers.remove(get_profile)
   profile.save() 

   return redirect(reverse('home-followers'))

@login_required
@transaction.atomic
def dislike(request, post_id):
   post = get_object_or_404(GrumblrPost, id=post_id)
   profile = get_object_or_404(GrumblrProfile, user=request.user)
   post.dislikes.add(profile)
   post.save()

   # Hacky sorry!
   return redirect(reverse('view-profile-user', 
      kwargs={'view_user' : request.user.username}))

@login_required
@transaction.atomic
def undislike(request, post_id):
   post = get_object_or_404(GrumblrPost, id=post_id)
   profile = get_object_or_404(GrumblrProfile, user=request.user)
   post.dislikes.remove(profile)
   post.save()

   return redirect(reverse('view-profile-user', 
      kwargs={'view_user' : request.user.username}))

@login_required
@transaction.atomic
def block(request, username):
   blocked_user = get_object_or_404(User, username=username)
   blocked_profile = get_object_or_404(GrumblrProfile, user=blocked_user)

   profile = get_object_or_404(GrumblrProfile, user=request.user)
   profile.blocked_users.add(blocked_profile)
   profile.save()
   
   return redirect(reverse('home-followers'))

# To use later
@login_required
@transaction.atomic
def unblock(request, username):
   unblocked_user = get_object_or_404(User, username=username)
   unblocked_profile = get_object_or_404(GrumblrProfile, user=unblocked_user)

   profile = get_object_or_404(GrumblrProfile, user=request.user)
   profile.blocked_users.remove(unblocked_profile)
   profile.save()

   return redirect(reverse('home-followers'))
   