from django.conf.urls import patterns, include, url
from grumblrapp.forms import LoginForm
from django.views.generic.base import TemplateView

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
   # ------------------------ home_views.py ------------------------
   url(r'^$', 'grumblrapp.home_views.home', name='home'),
   url(r'^followers$', 'grumblrapp.home_views.home_followers', name='home-followers'),
   # Route for built-in authentication with our own custom login page

   # ------------------------ register_views.py ------------------------
   url(r'^register$', 'grumblrapp.register_views.register_user', name='register'),
   url(r'^confirm-registration/(?P<username>[a-zA-Z0-9_@\+\-]+)/(?P<token>[a-z0-9\-]+)$', 
      'grumblrapp.register_views.confirm_registration', name='confirm'),
   url(r'^reset$', 'grumblrapp.register_views.reset', name='reset'),
   url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            'grumblrapp.register_views.reset_confirm', name='password_reset_confirm'),
   url(r'^success$', TemplateView.as_view(template_name='grumblrapp/reset-success.html'), name='success'),
   url(r'^success/email$', TemplateView.as_view(template_name='grumblrapp/reset-email.html'), name='reset-email'),

   # ------------------------ post_views.py ------------------------
   # Making a Grumblr post   
   url(r'^makepost-page$', 'grumblrapp.post_views.makepost_page', name='makepost-page'),
   url(r'^make-a-post$', 'grumblrapp.post_views.make_a_post', name='make-a-post'),
   url(r'^update-post/(?P<post_id>\d+)$', 'grumblrapp.post_views.update_posts', name='update-post'),
   # url(r'^post\.html$', TemplateView.as_view(template_name='grumblrapp/post_template.html')),

   # ------------------------ buttons_views.py ------------------------
   # Dislike/Un-dislike posts
   url(r'^dislike/(?P<post_id>\d+)$', 'grumblrapp.buttons_views.dislike', name='dislike'),
   url(r'^undislike/(?P<post_id>\d+)$', 'grumblrapp.buttons_views.undislike', name='undislike'),
   # Follow/Unfollow users
   url(r'^follow/(?P<username>\w+)$', 'grumblrapp.buttons_views.follow', name='follow'),
   url(r'^unfollow/(?P<username>\w+)$', 'grumblrapp.buttons_views.unfollow', name='unfollow'),
   # Block/Unblock users
   url(r'^block/(?P<username>\w+)$', 'grumblrapp.buttons_views.block', name='block'),
   url(r'^unblock/(?P<username>\w+)$', 'grumblrapp.buttons_views.unblock', name='unblock'),
   
   # ------------------------ comments_views.py ------------------------
   url(r'^get-comments/(?P<post_id>\d+)$', 'grumblrapp.comments_views.get_comments', name='get-comments'),
   url(r'^add-comment/(?P<post_id>\d+)$', 'grumblrapp.comments_views.add_comment', name='add-comment'),
   
   # ------------------------ profile_views.py ------------------------
   # Editing profile
   url(r'^edit-profile$', 'grumblrapp.profile_views.edit_profile', name='edit-profile'),
   url(r'^edit-profile-img$', 'grumblrapp.profile_views.edit_profile_img', name='edit-profile-img'),
   url(r'^edit-profile-email$', 'grumblrapp.profile_views.edit_profile_email', name='edit-profile-email'),
   url(r'^edit-profile-pw$', 'grumblrapp.profile_views.edit_profile_pw', name='edit-profile-pw'),
   url(r'^edit-profile-info$', 'grumblrapp.profile_views.edit_profile_info', name='edit-profile-info'),
   # View profile
   url(r'^view-profile/(?P<view_user>\w+)$', 'grumblrapp.profile_views.view_profile_user', name='view-profile-user'),
   
   # ------------------------ search_views.py ------------------------
   # Search Results
   url(r'^search-results$', 'grumblrapp.search_views.search_results', name='search-results'),

   # ------------------------ LOGIN/LOGOUT ------------------------
   url(r'^login$', 'django.contrib.auth.views.login', 
         {'template_name':'grumblrapp/login.html', 
         'authentication_form' : LoginForm}, 
         name='login'),
   # Route to logout a user and send them back to the login page
   url(r'^logout$', 'django.contrib.auth.views.logout_then_login', name='logout'), 
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
