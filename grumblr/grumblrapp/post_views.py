# For homepage, search results, and making posts
# ---------------------- LIBRARIES/IMPORTS ---------------------- 
import sys
import urllib
from itertools import chain
import buttons_views

from django.core import serializers
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.template import Context, loader

# Decorator to use built-in authentication system
from django.contrib.auth.decorators import login_required

# Used to create and manually log in a user
from django.contrib.auth.models import User

from grumblrapp.models import *
from grumblrapp.forms import *
# ----------------------------- MAIN -----------------------------
@login_required
@transaction.atomic
def makepost_page(request):
   profile = get_object_or_404(GrumblrProfile, user=request.user)
   return render(request, 'grumblrapp/make-post.html',
      {'form' : GrumblrPostForm(), 'profile' : profile})

@login_required
@transaction.atomic
def make_a_post(request):
   profile = get_object_or_404(GrumblrProfile, user=request.user)
   new_post = GrumblrPost(profile=profile)
   form = GrumblrPostForm(request.POST, request.FILES, instance=new_post)

   if (not form.is_valid()):
      return render(request, 'grumblrapp/make-post.html', {'form' : form,
         'profile' : profile})

   form.save()
   return redirect(reverse('home'))

@login_required
@transaction.atomic
# post_id is the latest post 
def update_posts(request, post_id):
   profile = get_object_or_404(GrumblrProfile, user=request.user)
   followers = profile.followers.all().exclude(id__in=profile.blocked_by.all()).values('id')
   response = []
   posts = GrumblrPost.objects.filter(id__gt=post_id).filter(profile__in=followers)
   new_posts = []
   
   for post in posts:
      new_posts.append((post,
      buttons_views.was_disliked(post, request.user)))
   
   context = {'posts' : new_posts, 'form' : GrumblrCommentForm(), 
      'profile' : profile}   
   return render(request, 'grumblrapp/update-posts.html', context)







