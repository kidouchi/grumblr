# For commenting functionality
# ---------------------- LIBRARIES/IMPORTS ---------------------- 
import json
import sys
import urllib
import home_views

from django.core import serializers
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import formats

# Decorator to use built-in authentication system
from django.contrib.auth.decorators import login_required

# Used to create and manually log in a user
from django.contrib.auth.models import User

# To manually create HttpResponses or raise Http404 exception
from django.http import HttpResponse, Http404

from grumblrapp.models import *
from grumblrapp.forms import *
# ----------------------------- MAIN -----------------------------
@login_required 
#note to self (to delete) : add comment button
@transaction.atomic
def add_comment(request, post_id):
   comment_form = GrumblrCommentForm(request.POST)
   post = GrumblrPost.objects.get(id=post_id)
   profile = get_object_or_404(GrumblrProfile, user=request.user)

   if(not comment_form.is_valid()):
      return get_comments(request, post_id)

   new_comment = comment_form.save(commit=False) # for the comment
   new_comment.post = post
   new_comment.profile = profile
   new_comment.username = request.user.username
   new_comment.save()
 
   return get_comments(request, post_id)

@login_required 
#note to self (to delete) : add comment button
@transaction.atomic
def get_comments(request, post_id):
   post = get_object_or_404(GrumblrPost, id=post_id)
   response = []
   for comment in GrumblrComment.objects.filter(post=post):
      response.append({
         'img': comment.profile.img.url, 
         'comment': comment.comment,
         'created_at': formats.date_format(comment.created_at, "DATETIME_FORMAT")
      })
   return HttpResponse(json.dumps(response), content_type="application/json")

