# For homepage, search results, and making posts
# ---------------------- LIBRARIES/IMPORTS ---------------------- 
import sys
import urllib
import buttons_views

from itertools import chain

from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.shortcuts import render, redirect, get_object_or_404

# Decorator to use built-in authentication system
from django.contrib.auth.decorators import login_required

# Used to create and manually log in a user
from django.contrib.auth.models import User

from grumblrapp.models import *
from grumblrapp.forms import *
# ----------------------------- MAIN -----------------------------
@login_required
@transaction.atomic
def home(request):
   profile = get_object_or_404(GrumblrProfile, user=request.user)
   posts = GrumblrPost.objects.filter(profile=profile)
 
   context = {'posts' : posts, 'profile' : profile, 
      'form' : GrumblrCommentForm()}
 
   return render(request, 'grumblrapp/home.html', context) 

@login_required
@transaction.atomic
def home_followers(request):
   profile = get_object_or_404(GrumblrProfile, user=request.user)
   your_followers = profile.followers.all().exclude(
      id__in=profile.blocked_by.all().values('id'))
   
   # Get posts from all the followers
   posts = [] 
   for follower in your_followers:
      posts = list(chain(posts, follower.post_profile.all()))

   comment_posts = []
   for post in posts:
      comment_posts.append(
         (post, #0
          buttons_views.was_disliked(post, request.user), #1
          buttons_views.was_blocked(profile, post.profile.user) #2
         ))

   context = {'posts' : comment_posts, 'form' : GrumblrCommentForm(),
      'profile' : profile}
   return render(request, 'grumblrapp/home-followers.html', context)








