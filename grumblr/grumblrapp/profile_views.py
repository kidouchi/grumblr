# For when editing/viewing profile page
# ---------------------- LIBRARIES/IMPORTS ---------------------- 
import sys
import urllib
import buttons_views

from itertools import chain

from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.shortcuts import render, redirect, get_object_or_404

# Decorator to use built-in authentication system
from django.contrib.auth.decorators import login_required

# For checking hashed passwords
from django.contrib.auth.hashers import check_password, make_password

# Used to create and manually log in a user
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate

from grumblrapp.models import *
from grumblrapp.forms import *
# ----------------------------- MAIN -----------------------------
@login_required 
@transaction.atomic
def view_profile_user(request, view_user):
   # Logged-in user
   logged_in_profile = get_object_or_404(GrumblrProfile, user=request.user)

   # Getting profile we want to view
   user = get_object_or_404(User, username=view_user)
   profile = get_object_or_404(GrumblrProfile, user=user)

   disliked_posts = profile.grumblrpost_set.all()
   is_owner = request.user.username == view_user
   
   comment_dislikes = []
   for post in disliked_posts:
      comment_dislikes.append(
         (post, 
          buttons_views.was_followed(logged_in_profile, post.profile.user)
         ))

   followers = profile.followers.all()

   context = {'profile' : profile, 
               'logged_in_profile' : logged_in_profile,
               'comment_dislikes' : comment_dislikes, 
               'is_owner' : is_owner,
               'username' : view_user,
               'pic_followers' : followers,
               'follow_count' : profile.followers.count(),
               'form' : GrumblrCommentForm()}
   return render(request, 'grumblrapp/view-profile.html', context)

@login_required
@transaction.atomic
def edit_profile(request):
   profile = get_object_or_404(GrumblrProfile, user=request.user)
   form = GrumblrProfileForm(instance=profile)
   
   return render(request, 'grumblrapp/edit-profile.html', 
      {'form' : form, 'profile' : profile})

@login_required
@transaction.atomic
def edit_profile_img(request):
   profile = get_object_or_404(GrumblrProfile, user=request.user)
   form = GrumblrProfileForm(request.POST, request.FILES, instance=profile)

   if (not form.is_valid()):
      return render(request, 'grumblrapp/edit-profile.html', 
         {'form' : form, 'profile' : profile})
  
   form.save()
   return redirect(reverse('view-profile-user', kwargs={'view_user' : request.user.username}))

@login_required
@transaction.atomic
def edit_profile_email(request):
   profile = get_object_or_404(GrumblrProfile, user=request.user)
   form = GrumblrProfileForm(request.POST, instance=profile)

   if (not form.is_valid()):
      return render(request, 'grumblrapp/edit-profile.html', 
         {'form' : form, 'profile' : profile})

   profile.email = form.cleaned_data['email1']
   profile.save()
   return redirect(reverse('view-profile-user', kwargs={'view_user' : request.user.username}))

@login_required
@transaction.atomic
def edit_profile_pw(request):
   profile = get_object_or_404(GrumblrProfile, user=request.user)
   form = GrumblrProfileForm(request.POST, instance=profile, user=request.user)

   if (not form.is_valid()):
      return render(request, 'grumblrapp/edit-profile.html', 
         {'form' : form, 'profile' : profile})

   grumblr_user = request.user
   clean_password = form.cleaned_data['password1']
   grumblr_user.password = make_password(clean_password)
   grumblr_user.save()
            
   auth_user = authenticate(username=request.user,
                            password=clean_password)
   login(request, auth_user)

   return redirect(reverse('view-profile-user', kwargs={'view_user' : request.user.username}))

@login_required
@transaction.atomic
def edit_profile_info(request):
   profile = get_object_or_404(GrumblrProfile, user=request.user)
   form = GrumblrProfileForm(request.POST, instance=profile)

   if (not form.is_valid()):
      return render(request, 'grumblrapp/edit-profile.html', 
         {'form' : form, 'profile' : profile})

   profile.age = form.cleaned_data['age']
   profile.birthday = form.cleaned_data['birthday']
   profile.self_description = form.cleaned_data['self_description']
   profile.save()

   return redirect(reverse('view-profile-user', kwargs={'view_user' : request.user.username}))

