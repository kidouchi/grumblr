import sys
from django.db import models

# User class for built-in authentication module
from django.contrib.auth.models import User

# Data model for user registration

class GrumblrProfile(models.Model):
   user = models.OneToOneField(User)
   img = models.ImageField(upload_to="user_pics", blank=True, default='/media/user_pics/user_default.png')
   email = models.EmailField(blank=True)
   age = models.PositiveSmallIntegerField(blank=True, null=True)
   birthday = models.DateField(blank=True, null=True) 
   self_description = models.CharField(max_length=400, blank=True)
   followers = models.ManyToManyField('self', blank=True, 
      related_name='following', symmetrical=False)
   blocked_users = models.ManyToManyField('self', blank=True, 
      related_name='blocked_by', symmetrical=False)

   def __unicode__(self):
      return self.user.username

class GrumblrPost(models.Model):
   profile = models.ForeignKey(GrumblrProfile, related_name="post_profile")
   title = models.CharField(max_length=100)
   image = models.ImageField(upload_to="grumblr_pics", blank=True, null=True)
   textpost = models.CharField(max_length=400)
   dislikes = models.ManyToManyField(GrumblrProfile, blank=True, default="",
      symmetrical=False)
   posted_at = models.DateTimeField(auto_now_add=True)

   def __unicode__(self):
      return self.profile.user.username

   @staticmethod
   def get_all_posts():
      return GrumblrPost.objects.all()     

   class Meta:
      ordering = ('-id',)

class GrumblrComment(models.Model):
   post = models.ForeignKey(GrumblrPost, null=True, on_delete=models.SET_NULL, related_name="comments")
   profile = models.ForeignKey(GrumblrProfile)
   comment = models.CharField(max_length=200)
   created_at = models.DateTimeField(auto_now_add=True)

   def __unicode__(self):
      return self.profile.user.username

   @staticmethod
   def get_comments(post):
      return GrumblrComment.objects.filter(post=post)


