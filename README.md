Grumblr was an academic project to create a web application much like Tumblr using Django and HTML/CSS/Javascript/jQuery

Running the project locally:

1. In the grumblr folder there is a grumblrapp folder. There should be a manage.py file.
2. Command line to run server $python manage.py runserver
3. Open your browser and go to: http://localhost:8000